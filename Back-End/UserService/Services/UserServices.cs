﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using UserService.Models;
using UserService.Models.DTO;

namespace UserService.Services
{
    public interface IUserService {
        Task<UserManagerResponse> RegisterUserAsync(RegisterUserDTO model);

        Task<UserManagerResponse> LoginAsync(LoginDTO model);
        Task<UserManagerResponse> RegisterAdminAsync(RegisterDTO model);

        Task<UserManagerResponse> EditUserAsync(UserDTO model);
    }
    public class UserServices : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;

        public UserServices(UserManager<User> userManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _configuration = configuration;
        }
        public async Task<UserManagerResponse> RegisterUserAsync(RegisterUserDTO model)
        {
            if (model == null)
                throw new NullReferenceException("Register Model is null");

            var identityUser = new User
            {
                Email = model.Email,
                UserName = model.Username,
                Address = model.Address,
                Password = model.Password,
            };

            var result = await _userManager.CreateAsync(identityUser, model.Password);
            await _userManager.AddToRoleAsync(identityUser, "User");

            var generatedGuid = Guid.NewGuid();

            if (result.Succeeded)
            {
                return new UserManagerResponse
                {
                    Message = "User created successfully!",
                    IsSuccess = true,
                    AppUser = identityUser
                };
            }

            return new UserManagerResponse
            {
                Message = "User did not create",
                IsSuccess = false,
                Errors = result.Errors.Select(e => e.Description)
            };
        }

        public async Task<UserManagerResponse> LoginAsync(LoginDTO model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null)
            {
                return new UserManagerResponse
                {
                    Message = "There is no user with that name.",
                    IsSuccess = false
                };
            }

            var result = await _userManager.CheckPasswordAsync(user, model.Password);
            if (!result)
            {
                return new UserManagerResponse
                {
                    Message = "Invalid password",
                    IsSuccess = false
                };
            }

            var role = await _userManager.GetRolesAsync(user);

            var claims = new[]
            {
                new Claim("Username", model.Username),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Role, role[0].ToString())
            };
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            var token = new JwtSecurityToken(
                expires: DateTime.Now.AddHours(1),
                claims: claims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
            );

            var redirectURL = "/user";

            if(role.Contains("Admin"))
            {
                redirectURL = "/admin";
            }

            string tokenizedString = new JwtSecurityTokenHandler().WriteToken(token);
            Console.WriteLine(tokenizedString);

            return new UserManagerResponse
            {
                Message = "User logged in successfully!",
                IsSuccess = true,
                Token = tokenizedString,
                ExpireDate = token.ValidTo,
                RedirectURL = redirectURL,
                AppUser = user
            };
        }

        public async Task<UserManagerResponse> EditUserAsync(UserDTO model)
        {
            var user = await _userManager.FindByIdAsync(model.Id);
            if (user == null)
            {
                return new UserManagerResponse
                {
                    Message = "There is no user with that name.",
                    IsSuccess = false
                };
            }

            user.Email = model.Email;
            user.UserName = model.Username;
            user.Address = model.Address;

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return new UserManagerResponse
                {
                    Message = "User updated successfully!",
                    IsSuccess = true,
                    AppUser = user
                };
            }

            return new UserManagerResponse
            {
                Message = "User did not update",
                IsSuccess = false,
                Errors = result.Errors.Select(e => e.Description)
            };

        }


        // ADMIN
        public async Task<UserManagerResponse> RegisterAdminAsync(RegisterDTO model)
        {
            if (model == null)
                throw new NullReferenceException("Register Model is null");

            var identityUser = new User
            {
                UserName = model.Username,
                Password = model.Password,
            };

            var result = await _userManager.CreateAsync(identityUser, model.Password);
            await _userManager.AddToRoleAsync(identityUser, "Admin");

            var generatedGuid = Guid.NewGuid();

            if (result.Succeeded)
            {
                return new UserManagerResponse
                {
                    Message = "Admin created successfully!",
                    IsSuccess = true
                };
            }

            return new UserManagerResponse
            {
                Message = "Admin did not create",
                IsSuccess = false,
                Errors = result.Errors.Select(e => e.Description)
            };
        }
    }
}

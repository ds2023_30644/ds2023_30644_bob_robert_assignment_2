﻿using Microsoft.AspNetCore.Mvc;
using UserService.Data;
using UserService.Models.DTO;
using UserService.Services;

namespace UserService.Controllers
{

    [ApiController]
    [Route("api/authenticate")]
    public class AuthenticateController : Controller
    {
        private readonly ApiDbContext _context;
        private readonly IUserService _userService;

        public AuthenticateController(
            ApiDbContext context,
            IUserService userService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> PostUser([FromBody] RegisterUserDTO registerDTO)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'ApiDbContext.Users'  is null.");
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.RegisterUserAsync(registerDTO);

            if (!result.IsSuccess)
            {
                return BadRequest(result);
            }

            return Ok();
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginDTO loginDTO)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.LoginAsync(loginDTO);

            if (!result.IsSuccess)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

    }
}

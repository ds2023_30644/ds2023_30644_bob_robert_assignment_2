using System.Net.Http.Headers;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using UserService.Data;
using UserService.Models;
using UserService.Models.DTO;
using UserService.Services;

namespace UserService.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly ApiDbContext _context;
        private readonly IUserService _userService;
        private readonly HttpClient client = new HttpClient();

        public UsersController(
            ILogger<UsersController> logger,
            ApiDbContext context,
            IUserService userService)
        {
            _logger = logger;
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));

            //client.BaseAddress = new Uri("http://localhost:32789/");
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            return await _context.Users.ToListAsync();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser([FromRoute] string id)
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] string id, [FromBody] UserDTO user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            var result = await _userService.EditUserAsync(user);

            if (!result.IsSuccess)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostUser([FromBody] RegisterUserDTO registerDTO)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'ApiDbContext.Users'  is null.");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.RegisterUserAsync(registerDTO);

            if (!result.IsSuccess)
            {
                return BadRequest(result);
            }

            //HttpResponseMessage response = await client.PostAsJsonAsync("api/devices_users", new RegisterUserDevicesDTO(result.AppUser));
            //response.EnsureSuccessStatusCode();

            return Ok(result);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] string? id)
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpPost("admin")]
        public async Task<IActionResult> PostAdmin([FromBody] RegisterDTO registerDTO)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'ApiDbContext.Users'  is null.");
            }
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var result = await _userService.RegisterAdminAsync(registerDTO);

            if (!result.IsSuccess)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        //private async Task<IActionResult> PostUserToDevices(RegisterUserDevicesDTO user)
        //{
        //    try
        //    {
        //        HttpClient client = new HttpClient
        //        {
        //            BaseAddress = new Uri("https://localhost:32780")
        //        };
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        HttpResponseMessage response = await client.PostAsJsonAsync("api/devices_users", user);
        //        response.EnsureSuccessStatusCode();

        //        return Ok(response);
        //    } catch (Exception e)
        //    {
        //        return BadRequest(e);
        //    }
        //}
    }
}
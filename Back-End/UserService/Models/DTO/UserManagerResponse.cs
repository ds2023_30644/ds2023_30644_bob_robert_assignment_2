namespace UserService.Models.DTO
{
    public class UserManagerResponse
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public IEnumerable<string> Errors { get; set; }
        public User AppUser { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDate { get; set; }
        public string RedirectURL { get; set; }
    }
}
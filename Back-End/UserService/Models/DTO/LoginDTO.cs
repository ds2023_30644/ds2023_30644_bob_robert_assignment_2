using System.ComponentModel.DataAnnotations;

namespace UserService.Models.DTO
{
    public class LoginDTO
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Password { get; set; }

    }
}
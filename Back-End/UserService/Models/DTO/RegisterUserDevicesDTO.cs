﻿namespace UserService.Models.DTO
{
    public class RegisterUserDevicesDTO
    {
        public string Id { get; set; }

        public RegisterUserDevicesDTO(User user)
        {
            Id = user.Id;
        }
    }
}

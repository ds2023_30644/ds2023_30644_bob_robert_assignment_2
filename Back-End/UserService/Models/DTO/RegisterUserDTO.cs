﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace UserService.Models.DTO
{
    public class RegisterUserDTO : RegisterDTO
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Address { get; set; }
    }
}

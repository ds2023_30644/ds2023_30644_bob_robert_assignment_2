﻿using Microsoft.AspNetCore.Identity;

namespace UserService.Models
{
    public class User : IdentityUser
    {
        public string Password { get; set; }

        public string Address { get; set; } = null!;

    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DeviceService3.Migrations
{
    /// <inheritdoc />
    public partial class removeForeignKey : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_device_user_UserId",
                table: "device");

            migrationBuilder.DropIndex(
                name: "IX_device_UserId",
                table: "device");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_device_UserId",
                table: "device",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_device_user_UserId",
                table: "device",
                column: "UserId",
                principalTable: "user",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DeviceService3.Migrations
{
    /// <inheritdoc />
    public partial class makeUserIdNull : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_device_user_UserId",
                table: "device");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "device",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddForeignKey(
                name: "FK_device_user_UserId",
                table: "device",
                column: "UserId",
                principalTable: "user",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_device_user_UserId",
                table: "device");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "device",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_device_user_UserId",
                table: "device",
                column: "UserId",
                principalTable: "user",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DeviceService3.Models.DTO
{
    public class DeviceDTO
    {
        public string Name { get; set; }
        public string? Description { get; set; }
        public string Address { get; set; }
        public int Hourly { get; set; }
        public string UserId { get; set; }
    }
}

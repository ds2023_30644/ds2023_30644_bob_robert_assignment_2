﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeviceService3.Models
{
    [Table("device")]
    public class Device
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("description")]
        public string? Description { get; set; }
        [Column("address")]
        public string Address { get; set; }
        [Column("hourly")]
        public int Hourly { get; set; }
        public string UserId { get; set; }
    }
}

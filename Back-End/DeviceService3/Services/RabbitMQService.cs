﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

using DeviceService3.Data;

namespace DeviceService3.Services
{
    public interface IRabbitMQService
    {
        void SendDevicesToRabbitMQ();
    }

    public class RabbitMQService : IRabbitMQService
    {
        private readonly AppDbContext _context;
        private readonly ConnectionFactory factory;
        private readonly ILogger logger;

        public RabbitMQService(AppDbContext context, LoggerFactory loggerFactory)
        {
            _context = context;
            factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };
            logger = loggerFactory.CreateLogger<RabbitMQService>();
            SendDevicesToRabbitMQ();
        }
        
        public void SendDevicesToRabbitMQ()
        {
            var devices = _context.Devices.ToList();

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: "devices.queue.log",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);

            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(devices));
            logger.LogInformation("Sending devices to RabbitMQ" + body);

            Console.WriteLine(Encoding.UTF8.GetString(body.ToArray()));
            channel.BasicPublish(exchange: "",
                                routingKey: "devices.queue.log",
                                basicProperties: null,
                                body: body);
        }
    }
}

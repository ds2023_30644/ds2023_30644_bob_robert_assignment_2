﻿using DeviceService3.Data;
using DeviceService3.Models;
using DeviceService3.Models.DTO;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace DeviceService3.Controllers
{
    [ApiController]
    [Route("api/devices")]
    public class DevicesController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ILogger logger;

        public DevicesController(AppDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            logger = loggerFactory.CreateLogger<DevicesController>();
            SendDevicesToRabbitMQ();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(string id)
        {
            var device = _context.Devices.Find(id);
            if (device == null)
            {
                return NotFound("device not found");
            }
            return Ok(device);
        }

        [HttpGet("user/{id}")]
        public IActionResult GetByUserId(string id)
        {
            var devices = _context.Devices.Where(d => d.UserId == id).ToList();
            return Ok(devices);
        }

        [HttpPost]
        public IActionResult Create(DeviceDTO deviceDTO)
        {
            var device = new Device();
            device.Name = deviceDTO.Name;
            device.Description = deviceDTO.Description;
            device.Address = deviceDTO.Address;
            device.UserId = deviceDTO.UserId;
            device.Hourly = deviceDTO.Hourly;

            _context.Devices.Add(device);
            _context.SaveChanges();
            return Ok(device);
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromRoute] string id, [FromBody] Device device)
        {
            var existingDevice = _context.Devices.Find(id);
            if (existingDevice == null)
            {
                return NotFound("device not found");
            }

            existingDevice.Name = device.Name;
            existingDevice.Description = device.Description;
            existingDevice.Address = device.Address;

            _context.Devices.Update(existingDevice);
            _context.SaveChanges();
            return Ok(existingDevice);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] string id)
        {
            var device = _context.Devices.Find(id);
            if (device == null)
            {
                return NotFound("device not found");
            }

            _context.Devices.Remove(device);
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet]
        public IActionResult GetAll() {
            var devices = _context.Devices.ToList();
            return Ok(devices);
        }

        [HttpPost("user/assign/{userId}")]
        public IActionResult AssignDevicesToUser([FromRoute] string userId, [FromBody] string[] deviceIds)
        {
            // var user = _context.Users.Find(userId);
            // if (user == null)
            // {
            //     return NotFound("user not found");
            // }

            if(userId == null) return BadRequest("userId is null");

            var devices = _context.Devices;

            foreach (var device in devices)
            {
                if (device.UserId == userId || device.UserId == "")
                {
                    if (deviceIds.Length > 0
                        && deviceIds.Contains(device.Id))
                    {
                        device.UserId = userId;
                    }
                    else
                    {
                        device.UserId = "";
                    }
                    _context.Devices.Update(device);
                }
            }

            _context.SaveChanges();
            return Ok();
        }

        private void SendDevicesToRabbitMQ()
        {
            var devices = _context.Devices.ToList();

            var factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://guest:guest@host.docker.internal:5672")
            };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "devices.queue.log",
                                        durable: false,
                                        autoDelete: false,
                                        exclusive: false);

                    //encode devices to json

                    
                    var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(devices));
                    logger.LogInformation("Sending devices to RabbitMQ" + body);

                    Console.WriteLine(Encoding.UTF8.GetString(body.ToArray()));
                    channel.BasicPublish(exchange: "",
                                        routingKey: "devices.queue.log",
                                        basicProperties: null,
                                        body: body);    
                }
            }
        }
    }
}

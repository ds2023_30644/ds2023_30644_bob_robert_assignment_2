﻿using DeviceService3.Models;
using Microsoft.EntityFrameworkCore;

namespace DeviceService3.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Device> Devices { get; set; }
    }
}

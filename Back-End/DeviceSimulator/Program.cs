﻿using DeviceSimulator;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

string deviceId = "test";
const string csvURL = "D:\\Files\\.Facultate\\SD\\Back-End\\DeviceSimulator\\sensor.csv";
const int interval = 2000;

var factory = new ConnectionFactory() {
    Uri = new Uri("amqp://guest:guest@localhost:5672")
};
using (var connection = factory.CreateConnection())
{
    using (var channel = connection.CreateModel())
    {
        channel.QueueDeclare(queue: "monitoring.queue.log",
                            durable: false,
                            autoDelete: false,
                            exclusive: false);
        //Read from CSV one by one
        using (TextFieldParser parser = new TextFieldParser(csvURL))
        {
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            var readingsCount = 0;
            DateTime startTime = DateTime.Now;

            while (!parser.EndOfData)
            {
                readingsCount++;
                DateTime currentTimestamp = generateTime(startTime, readingsCount);

                string[] fields = parser.ReadFields();

                SensorReading sensorReading = new SensorReading()
                {
                    DeviceId = deviceId,
                    Timestamp = currentTimestamp,
                    Value = double.Parse(fields[0])
                };
                Console.WriteLine($"({sensorReading.DeviceId},{sensorReading.Timestamp},{sensorReading.Value})");

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(sensorReading));
                Console.WriteLine(body);

                Console.WriteLine(Encoding.UTF8.GetString(body.ToArray()));
                channel.BasicPublish(exchange: "",
                                    routingKey: "monitoring.queue.log",
                                    basicProperties: null,
                                    body: body);
                Thread.Sleep(interval);
            }
        }
    }
}

DateTime generateTime (DateTime startTime, int readingsCount)
{
    return startTime.AddMinutes(readingsCount * 10);
}




using MonitoringService.Services;
using MonitoringService.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<MonitoringDbContext>(
    opt => opt.UseInMemoryDatabase("sensors"));

builder.Services.AddScoped<ISensorService, SensorService>();
builder.Services.AddScoped<IHourlyReadingService, HourlyReadingService>();
builder.Services.AddHostedService<ConsumeRabbitMQHostedService>();

builder.Services.AddCors(o =>
{
    o.AddPolicy("AllowOrigin", builder => {
        builder
            .WithOrigins("http://localhost:3000")
            .AllowAnyHeader()
            .AllowCredentials()
            .WithHeaders("Authorization", "Content-Type", "access-control-allow-origin")
            .WithExposedHeaders("Authorization")
            .AllowAnyMethod();
    });
});
builder.Services.AddSignalR();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
// pentru proiect
app.UseCors(x =>x.AllowAnyHeader().AllowAnyMethod().WithOrigins("http://localhost:3000"));

app.MapHub<NotificationsHub>("notifications");

// pana aici

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

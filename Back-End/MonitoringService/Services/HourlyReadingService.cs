﻿using Microsoft.EntityFrameworkCore;

using MonitoringService.Data;
using MonitoringService.Models;
using MonitoringService.Shared;

namespace MonitoringService.Services
{
    public interface IHourlyReadingService
    {
        Task<List<HourlyReading>> GetAll();
        Task<HourlyReadingResponse> Add(HourlyReading hourlyReading);
    }

    public class HourlyReadingService : IHourlyReadingService
    {
        private readonly MonitoringDbContext _context;

        public HourlyReadingService(MonitoringDbContext context)
        {
            _context = context;
        }

        public async Task<List<HourlyReading>> GetAll()
        {
            var hourlyReadings = await _context.HourlyReadings.ToListAsync();
            return hourlyReadings;
        }

        public async Task<HourlyReadingResponse> Add(HourlyReading hourlyReading)
        {
            try
            {
                var newHourlyReading = new HourlyReading
                {
                    Id = Guid.NewGuid().ToString(),
                    DeviceId = hourlyReading.DeviceId,
                    Timestamp = hourlyReading.Timestamp,
                    Value = hourlyReading.Value
                };

                await _context.HourlyReadings.AddAsync(newHourlyReading);
                await _context.SaveChangesAsync();
                return new HourlyReadingResponse { IsSuccess = true, HourlyReadings = new List<HourlyReading> { newHourlyReading } };
            }
            catch (Exception ex)
            {
                return new HourlyReadingResponse { IsSuccess = false, Message = ex.Message };
            }
        }
    }
}

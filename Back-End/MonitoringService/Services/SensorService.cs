using Microsoft.EntityFrameworkCore;

using MonitoringService.Data;
using MonitoringService.Models;
using MonitoringService.Shared;
using System.Collections.Generic;

namespace MonitoringService.Services
{
    public interface ISensorService {
        Task<SensorResponse> GetAll();
        Task<SensorResponse> Add(SensorReading sensorReading);
        Task<SensorResponse> GetByDeviceIdAndHour(string deviceId, int hour);
        Task<SensorResponse> GetLastReadingInHour(string deviceId, int hour, int day, int month, int year);
    }

    public class SensorService : ISensorService {
        private readonly MonitoringDbContext _context;

        public SensorService(MonitoringDbContext context)
        {
            _context = context;
        }

        public async Task<SensorResponse> GetAll()
        {
            var sensors = await _context.SensorReadings.ToListAsync();
            return new SensorResponse { IsSuccess = true, SensorReadings = sensors };
        }

        public async Task<SensorResponse> Add(SensorReading sensorReading)
        {
            try
            {
                var sensor = new SensorReading
                {
                    Id = Guid.NewGuid().ToString(),
                    DeviceId = sensorReading.DeviceId,
                    Timestamp = sensorReading.Timestamp,
                    Value = sensorReading.Value
                };

                await _context.SensorReadings.AddAsync(sensor);
                await _context.SaveChangesAsync();
                return new SensorResponse { IsSuccess = true, SensorReadings = new List<SensorReading> { sensor } };
            }
            catch (Exception ex)
            {
                return new SensorResponse { IsSuccess = false, Message = ex.Message };
            }
        }

        public async Task<SensorResponse> GetByDeviceIdAndHour(string deviceId, int hour)
        {
            var sensors = await _context.SensorReadings.Where(x => x.DeviceId == deviceId && x.Timestamp.Hour == hour).ToListAsync();
            return new SensorResponse { IsSuccess = true, SensorReadings = sensors };
        }

        public async Task<SensorResponse> GetLastReadingInHour(string deviceId, int hour, int day, int month, int year)
        {
            SensorReading latestSensor = await _context.SensorReadings.LastOrDefaultAsync(x => x.DeviceId == deviceId && x.Timestamp.Hour == hour && x.Timestamp.Day == day && x.Timestamp.Month == month && x.Timestamp.Year == year);
            List<SensorReading> sensorList = new List<SensorReading> { latestSensor };
            return new SensorResponse { IsSuccess = true, SensorReadings = sensorList };
        }
    }
}
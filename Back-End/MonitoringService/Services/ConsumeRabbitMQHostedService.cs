using Microsoft.Extensions.Logging;
using MonitoringService.Data;
using MonitoringService.Models;
using MonitoringService.Shared;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading.Channels;

namespace MonitoringService.Services
{
    public class ConsumeRabbitMQHostedService : BackgroundService
    {
        private readonly ILogger _logger;
        ConnectionFactory factory;
        IConnection conn;
        IModel channel;

        private readonly IServiceScopeFactory _scopeFactory;

        private DateTime currentHour;

        public ConsumeRabbitMQHostedService (ILoggerFactory loggerFactory, 
            IServiceScopeFactory serviceScopeFactory)
        {
            _logger = loggerFactory.CreateLogger<ConsumeRabbitMQHostedService>();
            _scopeFactory = serviceScopeFactory;

            currentHour = DateTime.Now;

            InitRabbitMQ();
        }

        private void InitRabbitMQ()
        {
            factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://guest:guest@host.docker.internal:5672")
            };

            conn = factory.CreateConnection();
            channel = conn.CreateModel();

            channel.QueueDeclare(queue: "monitoring.queue.log",
                                 durable: false,
                                 autoDelete: false,
                                 exclusive: false);
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            ConsumeSensorQueue();

            //ConsumeDevicesQueue();

            return Task.CompletedTask;
        }

        private void ConsumeSensorQueue()
        {
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                // received message
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                if (content == null)
                {
                    _logger.LogInformation("content is null");
                }
                if (content[0] == 'D')
                {
                    _logger.LogInformation("content is D");
                }
                else
                {
                    SensorReadingDTO sensorReading = JsonConvert.DeserializeObject<SensorReadingDTO>(content);

                    // handle the received message 
                    HandleMessageAsync(sensorReading);
                    channel.BasicAck(ea.DeliveryTag, multiple: false);
                }
            };

            channel.BasicConsume(queue: "monitoring.queue.log",
                                 autoAck: false,
                                 consumer: consumer);

            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerConsumerCancelled;
        }   

        //private void ConsumeDevicesQueue()
        //{
        //    var consumer = new EventingBasicConsumer(channel);
        //    consumer.Received += (model, ea) =>
        //    {
        //        // received message
        //        var content = Encoding.UTF8.GetString(ea.Body.ToArray());
        //        if (content == null)
        //        {
        //            _logger.LogInformation("content is null");
        //        }
        //        if (content[0] == 'D')
        //        {
        //            _logger.LogInformation("content is D");
        //        }
        //        else
        //        {
        //            DeviceDTO device = JsonConvert.DeserializeObject<DeviceDTO>(content);

        //            // handle the received message 
        //            //HandleMessageAsync(device);
        //            channel.BasicAck(ea.DeliveryTag, multiple: false);
        //        }
        //    };

        //    channel.BasicConsume(queue: "devices.queue.log",
        //                         autoAck: false,
        //                         consumer: consumer);

        //    consumer.Shutdown += OnConsumerShutdown;
        //    consumer.Registered += OnConsumerRegistered;
        //    consumer.Unregistered += OnConsumerUnregistered;
        //    consumer.ConsumerCancelled += OnConsumerConsumerCancelled;
        //}

        private async Task HandleMessageAsync(SensorReadingDTO reading)
        {
            _logger.LogInformation($"consumer received {reading.DeviceId}, {reading.Timestamp}, {reading.Value}");
            
            using var scope = _scopeFactory.CreateScope();
            var _sensorService = scope.ServiceProvider.GetRequiredService<ISensorService>();
            await _sensorService.Add(new SensorReading
            {
                Id = Guid.NewGuid().ToString(),
                DeviceId = reading.DeviceId,
                Timestamp = reading.Timestamp,
                Value = reading.Value
            });

            if (reading.Timestamp.Hour != currentHour.Hour)
            {
                ComputeLastHourAverageValueAsync(reading.DeviceId, reading.Timestamp);
                currentHour = reading.Timestamp;
                _logger.LogInformation($"new hour: {currentHour}");
            }
        }

        private async Task ComputeLastHourAverageValueAsync(string deviceId, DateTime timestamp)
        {
            using var scope = _scopeFactory.CreateScope();
            var _sensorService = scope.ServiceProvider.GetRequiredService<ISensorService>();
            var _hourlyReadingService = scope.ServiceProvider.GetRequiredService<IHourlyReadingService>();

            _logger.LogInformation($"ComputeLastHourAverageValue for {deviceId}, {timestamp}");
            var sensorReadings = _sensorService.GetByDeviceIdAndHour(deviceId, timestamp.Hour - 1);

            if (!sensorReadings.Result.IsSuccess || sensorReadings.Result.SensorReadings == null)
            {
                _logger.LogInformation("getting devices failed");
            }
            else if (sensorReadings.Result.IsSuccess)
            {
                var previousHourLatestReading = await _sensorService.GetLastReadingInHour(deviceId, timestamp.Hour - 2, timestamp.Day, timestamp.Month, timestamp.Year);
                var previousHourLatestValue = 0.0;
                if (previousHourLatestReading.SensorReadings[0] != null)
                {
                    previousHourLatestValue = previousHourLatestReading.SensorReadings[0].Value;
                }
                var hourlyConsumption = sensorReadings.Result.SensorReadings.Last().Value - previousHourLatestValue;


                await _hourlyReadingService.Add(new HourlyReading
                {
                    Id = Guid.NewGuid().ToString(),
                    DeviceId = deviceId,
                    Timestamp = timestamp,
                    Value = hourlyConsumption
                }); ;
                
            }
        }

        public override void Dispose()
        {
            channel.Close();
            conn.Close();
            base.Dispose();
        }

        private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e) { }
        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) { }
        private void OnConsumerRegistered(object sender, ConsumerEventArgs e) { }
        private void OnConsumerShutdown(object sender, ShutdownEventArgs e) { }
        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) { }
    }
}

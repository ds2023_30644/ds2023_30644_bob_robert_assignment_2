﻿using Microsoft.AspNetCore.SignalR;
using MonitoringService.Models;

namespace MonitoringService.Services
{
    public class NotificationsHub : Hub<INotificationClient>
    {
        public override async Task OnConnectedAsync()
        {
            await Clients.Client(Context.ConnectionId).ReceiveMessage(new NotificationMessage { Message = $"Connected {Context.User?.Identity?.Name}" });
            await base.OnConnectedAsync();
        }
    }

    public interface INotificationClient
    {
        Task ReceiveMessage(NotificationMessage message);
    }
}

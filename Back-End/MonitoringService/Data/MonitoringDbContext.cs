using MonitoringService.Models;
using Microsoft.EntityFrameworkCore;

namespace MonitoringService.Data
{
	public class MonitoringDbContext : DbContext
	{
		public MonitoringDbContext(DbContextOptions<MonitoringDbContext> options) : base (options)
		{
		}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<SensorReading> SensorReadings { get; set; }
        public DbSet<HourlyReading> HourlyReadings { get; set; }
        public DbSet<Measurement> Measurements{ get; set; }
    }
}


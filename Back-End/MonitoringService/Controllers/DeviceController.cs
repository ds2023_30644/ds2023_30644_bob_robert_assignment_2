﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;

namespace MonitoringService.Controllers
{
    public class DeviceController : Controller
    {
        [HttpGet]
        [Route("api/device")]
        public IActionResult GetById(string Id)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri($"https://172.17.0.3:32770");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync($"api/devices/{Id}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var device = response.Content.ToString();
                    return Ok(device);
                }
                return BadRequest(response.StatusCode);
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

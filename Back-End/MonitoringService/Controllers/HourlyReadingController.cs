﻿using Microsoft.AspNetCore.Mvc;

using MonitoringService.Services;

namespace MonitoringService.Controllers
{
    public class HourlyReadingController : Controller
    {
        private readonly IHourlyReadingService _hourlyReadingsService;

        public HourlyReadingController(IHourlyReadingService hourlyReadingsService)
        {
            _hourlyReadingsService = hourlyReadingsService;
        }

        [HttpGet]
        [Route("api/hourly-reading")]
        public async Task<IActionResult> GetAll()
        {
            var hourlyReadings = await _hourlyReadingsService.GetAll();
            return Ok(hourlyReadings);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using MonitoringService.Models;
using MonitoringService.Services;

namespace MonitoringService.Controllers
{
    public class SensorReadingController : Controller
    {
        private readonly ISensorService _sensorService;

        public SensorReadingController(ISensorService sensorService)
        {
            _sensorService = sensorService;
        }

        [HttpGet]
        [Route("api/sensor-reading")]
        public async Task<IActionResult> GetAll()
        {
            var response = await _sensorService.GetAll();
            if (response.IsSuccess)
            {
                return Ok(response.SensorReadings);
            }
            return NotFound();
        }

        [HttpPost]
        [Route("api/sensor-reading")]
        public async Task<IActionResult> Add([FromBody] SensorReading sensorReading)
        {
            var response = await _sensorService.Add(sensorReading);
            if (response.IsSuccess)
            {
                return Ok(response.SensorReadings);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("api/sensor-reading/{deviceId}/{hour}")]
        public async Task<IActionResult> GetByDeviceIdAndHour(string deviceId, int hour)
        {
            var response = await _sensorService.GetByDeviceIdAndHour(deviceId, hour);
            if (response.IsSuccess)
            {
                return Ok(response.SensorReadings);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("api/sensor-reading/{deviceId}/{hour}/{day}/{year}/last")]
        public async Task<IActionResult> GetLastReadingInHour(string deviceId, int hour, int day, int month, int year)
        {
            var response = await _sensorService.GetLastReadingInHour(deviceId, hour, day, month, year);
            if (response.IsSuccess)
            {
                return Ok(response.SensorReadings);
            }
            return NotFound();
        }
    }
}

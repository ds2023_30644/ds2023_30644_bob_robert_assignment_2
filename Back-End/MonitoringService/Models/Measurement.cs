﻿namespace MonitoringService.Models
{
    public class Measurement
    {
        public string Id { get; set; }
        public string DeviceId { get; set; }
        public double EnergyReading { get; set; }
        public double MaxValue { get; set; }
        public DateTime Timestamp { get; set; }

    }
}

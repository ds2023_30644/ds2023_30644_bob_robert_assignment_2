﻿namespace MonitoringService.Models
{
    public class SensorReading
    {
        public string Id { get; set; }
        public string DeviceId { get; set; }
        public DateTime Timestamp { get; set; }
        public double Value { get; set; }
    }
}

﻿namespace MonitoringService.Models
{
    public class NotificationMessage
    {
        public string Message { get; set; }
        public string DeviceId { get; set; }
        public string Timestamp { get; set; }
    }
}

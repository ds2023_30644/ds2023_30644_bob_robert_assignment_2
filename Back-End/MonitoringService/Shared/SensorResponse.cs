﻿using MonitoringService.Models;

namespace MonitoringService.Shared
{
    public class SensorResponse
    {
        public string? Message { get; set; }
        public bool IsSuccess { get; set; }
        public IEnumerable<string>? Errors { get; set; }
        public List<SensorReading>? SensorReadings { get; set; }
    }
}

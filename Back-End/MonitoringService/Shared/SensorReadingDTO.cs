﻿namespace MonitoringService.Shared
{
    public class SensorReadingDTO
    {
        public string DeviceId { get; set; }
        public DateTime Timestamp { get; set; }
        public double Value { get; set; }
    }
}

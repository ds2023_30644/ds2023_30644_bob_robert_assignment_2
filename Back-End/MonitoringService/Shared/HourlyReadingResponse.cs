﻿using MonitoringService.Models;

namespace MonitoringService.Shared
{
    public class HourlyReadingResponse
    {
        public string? Message { get; set; }
        public bool IsSuccess { get; set; }
        public IEnumerable<string>? Errors { get; set; }
        public List<HourlyReading>? HourlyReadings { get; set; }
    }
}

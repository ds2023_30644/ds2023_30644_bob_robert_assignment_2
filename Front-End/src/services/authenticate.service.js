const BASE_URL = 'https://localhost:32768/api/authenticate';

class AuthService {
    login = (username, password) => {
        const url = `${BASE_URL}/login`;
        const requestOptions = {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username, password })
        };

        console.log('username, password', username, password)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }

                return response.json();
            })
            .then(user => {
                if (user && user.token) {
                    localStorage.setItem('user', JSON.stringify(user));
                }

                return user;
            });
    }

    register = (userData) => {
        const url = `${BASE_URL}`;
        const requestOptions = {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response;
            })
            .then(response => response.json());
    }
}

export default AuthService;
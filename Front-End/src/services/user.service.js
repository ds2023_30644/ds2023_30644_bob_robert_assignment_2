const BASE_URL = 'https://localhost:32768/api/users';

class UserService {
    register = (userData) => {
        const url = `${BASE_URL}`;
        const requestOptions = {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response.json();
            });
    }

    update = (userData) => {
        const url = `${BASE_URL}/${userData.id}`;
        const requestOptions = {
            method: 'PUT',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response.json();
            });
    }

    delete = (id) => {
        const url = `${BASE_URL}/${id}`;
        const requestOptions = {
            method: 'DELETE',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            }
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response;
            });
    }

    getAll = () => {
        const url = `${BASE_URL}`;
        const requestOptions = {
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            }
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response.json();
            });
    }

    registerAdmin = (userData) => {
        const url = `${BASE_URL}/admin`;
        const requestOptions = {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response.json();
            });
    }


}

export default UserService;
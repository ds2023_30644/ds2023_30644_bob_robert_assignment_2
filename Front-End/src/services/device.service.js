const BASE_URL = 'https://localhost:32770/api/devices';

class DeviceService {
    getDevicesOfUser = (userId) => {
        const url = `${BASE_URL}/user/${userId}`;
        const requestOptions = {
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            }
        };

        console.log('userId', userId)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }

                return response.json();
            });
    }

    create = (device) => {
        const url = `${BASE_URL}`;
        const requestOptions = {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(device)
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response;
            });
    };

    update = (device) => {
        device.userId = device.userId || '';
        const url = `${BASE_URL}/${device.id}`;
        const requestOptions = {
            method: 'PUT',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(device)
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response;
            });
    }

    delete = (deviceId) => {
        const url = `${BASE_URL}/${deviceId}`;
        const requestOptions = {
            method: 'DELETE',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            }
        };

        console.log('url', url)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response;
            });
    };

    getAll = () => {
        const url = `${BASE_URL}`;
        const requestOptions = {
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            }
        };
        console.log('getAllDevices url', url)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }

                return response.json();
            });
    }

    assignDevicesToUser = (userId, deviceIds) => {
        const url = `${BASE_URL}/user/assign/${userId}`;
        const requestOptions = {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(deviceIds)
        };

        console.log('url', url)
        console.log('requestOptions', requestOptions)

        return fetch(url, requestOptions)
            .then(response => {
                console.log('response', response)
                if (!response.ok) {
                    return Promise.reject(response.statusText);
                }
                return response;
            });
    }
}

export default DeviceService;
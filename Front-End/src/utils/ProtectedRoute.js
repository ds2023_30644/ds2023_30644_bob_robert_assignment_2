import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ProtectedRoute = ({path, component}) => {
    const navigate = useNavigate();
    const [isAuthenticated, setAuthentication] = useState(false);

    const checkUser = () => {
        const user = JSON.parse(localStorage.getItem('user'));
        if (!user) {
            setAuthentication(false);
            return navigate('/');
        }
        console.log('user', user)
        const userRole = user.address ? 'user' : 'admin';
        if (userRole === 'admin' && path === '/admin') {
            setAuthentication(true);
            return;
        }
        if (userRole === 'user' && path === '/user') {
            setAuthentication(true);
            return;
        }
        return navigate('/');
    }
    useEffect(() => {
            checkUser();
    }, [path]);

    return (
        <React.Fragment>
            {
                isAuthenticated ? component : null
            }
        </React.Fragment>
    );
}
export default ProtectedRoute;
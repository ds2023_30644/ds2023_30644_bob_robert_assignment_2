import { Routes, Route } from "react-router-dom";
import './App.css';

import Header from "./components/Header";
import Login from './components/Login';
import Register from './components/Register';
import UserHome from './components/user/UserHome';
import AdminHome from './components/admin/AdminHome';

import ProtectedRoute from './utils/ProtectedRoute';

function App() {
  return (
    <div className="App">
      <Header />

      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/user" element={
            <ProtectedRoute path={"/user"} component={<UserHome />} />
          } />
        <Route path="/admin" element={
            <ProtectedRoute path={"/admin"} component={<AdminHome />} />
          } />
        <Route path="/register" element={<Register />} />
      </Routes>
    </div>
  );
}

export default App;

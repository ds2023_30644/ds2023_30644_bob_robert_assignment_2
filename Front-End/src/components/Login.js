import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, TextField, Button, Box } from '@material-ui/core';
import { useNavigate } from 'react-router-dom';

import AuthService from '../services/authenticate.service';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center', // Center horizontally
        alignItems: 'center', // Center vertically
        height: '100vh', // Set height to fill the viewport
    },
    formBox: {
        width: '100%',
        maxWidth: '400px', // Set a maximum width for the form box
    },
    button: {
        margin: theme.spacing(1),
    },
}));

const service = new AuthService();

const Login = () => {
    const classes = useStyles();
    const [error, setError] = React.useState(false);
    const navigate = useNavigate();

    const [userResponse, setUserResponse] = useState({});

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        service.login(data.get('username'), data.get('password'))
            .then(response => {
                if (response) {
                    setError(false);
                    localStorage.setItem('user', JSON.stringify(response.appUser));
                    console.log('user', localStorage.getItem('user'))
                    setUserResponse(response);
                } else {
                    throw new Error("response not received");
                }
            })
            .catch(e => {
                console.log(e);
                setError(true);
            });
    };

    useEffect(() => {
        navigate(userResponse.redirectURL);
        // navigate('/admin');
    }, [userResponse]);

    return (
        <Container className={classes.root}>
            <Box
                maxWidth="xs"
                className={classes.root}
                component="form"
                onSubmit={handleSubmit}
                noValidate
                sx={{
                    mt: 1}}>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="username"
                    label="Username"
                    name="username"
                    autoComplete="username"
                    autoFocus
                    error={error}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    error={error}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Log In
                </Button>

                <Button variant="contained" color="primary" href="/register" className={classes.button}>
                    Register
                </Button>
            </Box>
        </Container>

    );
};

export default Login;

import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, TextField, Button, Box } from '@material-ui/core';
import { useNavigate } from 'react-router-dom';

import UserService from '../services/user.service';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center', // Center horizontally
        alignItems: 'center', // Center vertically
        height: '100vh', // Set height to fill the viewport
    },
    formBox: {
        width: '100%',
        maxWidth: '400px', // Set a maximum width for the form box
    },
    button: {
        margin: theme.spacing(1),
    },
}));

const service = new UserService();

const Register = () => {
    const classes = useStyles();
    const [error, setError] = React.useState(false);
    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);

        const registerData = {
            Username: data.get('username'),
            Email: data.get('email'),
            Address: data.get('address'),
            Password: data.get('password')
        };

        if (registerData.Username === '' || registerData.Email === '' || registerData.Address === '' || registerData.Password === '') {
            setError(true);
            return;
        }

        service.register(registerData)
            .then(response => {
                if (response.isSuccess) {
                    setError(false);
                    navigate('/');
                } else {
                    throw new Error(response.status);
                }
            })
            .catch(e => {
                console.log('register error', e);
                setError(true);
            });
    };


    return (
        <Container className={classes.root}>
            <Box
                maxWidth="xs"
                className={classes.root}
                component="form"
                onSubmit={handleSubmit}
                noValidate
                sx={{
                    mt: 1}}>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="username"
                    label="Username"
                    name="username"
                    autoComplete="username"
                    autoFocus
                    error={error}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    error={error}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="address"
                    label="Address"
                    name="address"
                    autoComplete="address"
                    autoFocus
                    error={error}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    error={error}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                >
                    Register
                </Button>
            </Box>
        </Container>
    );
};

export default Register;

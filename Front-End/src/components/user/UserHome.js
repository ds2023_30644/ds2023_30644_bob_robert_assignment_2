import React, { useEffect, useState } from 'react';
import { Box, TextField, Button } from '@material-ui/core';
import DeviceTable from './DeviceTable';

import DeviceService from '../../services/device.service';

const deviceService = new DeviceService();

const UserHome = () => {
    const [user, setUser] = useState({ username: '' });
    const [devices, setDevices] = useState([]);

    useEffect(() => {
        const userFromLocalStorage = JSON.parse(localStorage.getItem('user'));
        console.log('userFromLocalStorage', userFromLocalStorage);
        setUser(userFromLocalStorage);
    }, []);

    useEffect(() => {
        deviceService.getDevicesOfUser(user.id).then((response) => {
            console.log('response', response);
            setDevices(response);
        });
    }, [user]);

    return (
        <Box>
            <h1>Welcome {user.userName}</h1>
            <p>Username: {user.userName}</p>
            <p>Password: {user.password}</p>
            <p>Email: {user.email}</p>
            <p>Address: {user.address}</p>

            <DeviceTable devices={devices} />
        </Box>
    );
};

export default UserHome;

import React, { useEffect, useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import DeviceService from "../../services/device.service";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const deviceService = new DeviceService();

function createData(name, description, address, hourly) {
    return { name, description, address, hourly };
}

const DeviceTable = ({ devices }) => {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell >Description</TableCell>
                        <TableCell >Address</TableCell>
                        <TableCell >Hourly</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {devices.map((device) => (
                        <TableRow key={device.name}>
                            <TableCell component="th" scope="row">
                                {device.name}
                            </TableCell>
                            <TableCell >{device.description}</TableCell>
                            <TableCell >{device.address}</TableCell>
                            <TableCell >{device.hourly}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default DeviceTable;
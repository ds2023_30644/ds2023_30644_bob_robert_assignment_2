import React from 'react';
import { AppBar, Box, Toolbar, Button } from "@material-ui/core";
import { useNavigate } from 'react-router-dom';

const Header = () => {
    const navigate = useNavigate();

    const onClick = () => {
        navigate('/');
    }
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Button
                        color="inherit"
                        onClick={onClick}>Login</Button>
                </Toolbar>
            </AppBar>
        </Box>
    );
}

export default Header;

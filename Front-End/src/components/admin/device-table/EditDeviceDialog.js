
import React, { useState, useEffect } from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
} from '@material-ui/core';

import DeviceService from '../../../services/device.service';

const deviceService = new DeviceService();

const EditDeviceDialog = ({ open, onClose, device }) => {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [address, setAddress] = useState('');
    const [hourly, setHourly] = useState('');
    const [userId, setUserId] = useState('');

    useEffect(() => {
        if (device !== null) {
            setId(device.id);
            setName(device.name);
            setDescription(device.description);
            setAddress(device.address);
            setHourly(device.hourly);
            setUserId(device.userId);
        }
    }, [device]);

    const handleDevicenameChange = (event) => {
        setName(event.target.value);
    };

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
    };

    const handleAddressChange = (event) => {
        setAddress(event.target.value);
    };

    const handleUserIdChange = (event) => {
        setUserId(event.target.value);
    };

    const handleSubmit = () => {
        // Handle form submission here
        console.log('Submitted:', { id, name, description, address });
        deviceService.update({ id, name, description, address, userId });
        onClose();
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Edit Device</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    label="Name"
                    type="text"
                    fullWidth
                    value={name}
                    onChange={handleDevicenameChange}
                />
                <TextField
                    margin="dense"
                    label="Description"
                    type="description"
                    fullWidth
                    value={description}
                    onChange={handleDescriptionChange}
                />
                <TextField
                    margin="dense"
                    label="Address"
                    type="text"
                    fullWidth
                    value={address}
                    onChange={handleAddressChange}
                />
                <TextField
                    margin="dense"
                    label="Hourly consumption"
                    type="number"
                    fullWidth
                    disabled={true}
                    value={hourly}
                />
                <TextField
                    margin="dense"
                    label="User Id"
                    type="text"
                    fullWidth
                    // disabled={true}
                    value={userId}
                    onChange={handleUserIdChange}
                />

            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                <Button onClick={handleSubmit}
                    color="primary"
                    disabled={!name || !description || !address}
                    >Edit</Button>
            </DialogActions>
        </Dialog>
    );
};

export default EditDeviceDialog;

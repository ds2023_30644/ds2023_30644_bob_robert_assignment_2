
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import Button from '@mui/material/Button'
import EditDeviceDialog from './EditDeviceDialog';

import DeviceService from '../../../services/device.service';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const deviceService = new DeviceService();

const DeviceTableAdmin = ({ devices }) => {
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);
    const [selectedDevice, setSelectedDevice] = React.useState(null);

    const handleEditClick = (device) => {
        setSelectedDevice(device);
        setOpen(true);
    };

    const handleDeleteClick = (device) => {
        deviceService.delete(device.id);
    };

    const handleClose = () => {
        setSelectedDevice(null);
        setOpen(false);
    };

    return (
    <>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="device table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Address</TableCell>
              <TableCell>Hourly</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {devices.map((device) => (
              <TableRow key={device.id}>
                <TableCell component="th" scope="row">
                  {device.name}
                </TableCell>
                <TableCell>{device.description}</TableCell>
                <TableCell>{device.address}</TableCell>
                <TableCell>{device.hourly}</TableCell>
                <TableCell>
                    <Button variant="contained" color="primary" onClick={() => handleEditClick(device)}>
                        Edit
                    </Button>
                </TableCell>

                <TableCell>
                    <Button variant="contained" color="error" onClick={() => handleDeleteClick(device)}>
                        Delete
                    </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <EditDeviceDialog open={open} onClose={handleClose} device={selectedDevice} />
    </>
  );
};

export default DeviceTableAdmin;


import React, { useState, useEffect } from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
} from '@material-ui/core';

import DeviceService from '../../../services/device.service';

const deviceService = new DeviceService();

const CreateDeviceDialog = ({ open, onClose }) => {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [address, setAddress] = useState('');

    const handleDevicenameChange = (event) => {
        setName(event.target.value);
    };

    const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
    };

    const handleAddressChange = (event) => {
        setAddress(event.target.value);
    };

    const handleSubmit = () => {
        // Handle form submission here
        console.log('Submitted:', { name, description, address });
        deviceService.create({ name, description, address, hourly: 0, userId: '' });
        onClose();
    };

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Create Device</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    label="Name"
                    type="text"
                    fullWidth
                    value={name}
                    onChange={handleDevicenameChange}
                />
                <TextField
                    margin="dense"
                    label="Description"
                    type="description"
                    fullWidth
                    value={description}
                    onChange={handleDescriptionChange}
                />
                <TextField
                    margin="dense"
                    label="Address"
                    type="text"
                    fullWidth
                    value={address}
                    onChange={handleAddressChange}
                />

            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                <Button onClick={handleSubmit}
                    color="primary"
                    disabled={!name || !description || !address}
                    >Create</Button>
            </DialogActions>
        </Dialog>
    );
};

export default CreateDeviceDialog;

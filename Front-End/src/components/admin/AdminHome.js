
import React, { useState, useEffect } from 'react';
import { Box, Button } from '@material-ui/core';
import DeviceTableAdmin from './device-table/DeviceTableAdmin';
import CreateUserDialog from './user-table/CreateUserDialog';
import UserTable from './user-table/UserTable';

import UserService from '../../services/user.service';
import DeviceService from '../../services/device.service';
import CreateDeviceDialog from './device-table/CreateDeviceDialog';

const userService = new UserService();
const deviceService = new DeviceService();

const AdminHome = () => {
    const [user, setUser] = useState({ username: '' });
    const [users, setUsers] = useState([]);
    const [open, setOpen] = useState(false);

    const [devices, setDevices] = useState([]);
    const [openCreateDevice, setOpenCreateDevice] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpenCreateDevice = () => {
        setOpenCreateDevice(true);
    };

    const handleCloseCreateDevice = () => {
        setOpenCreateDevice(false);
    };

    useEffect(() => {
        const userFromLocalStorage = JSON.parse(localStorage.getItem('user'));
        setUser(userFromLocalStorage);
    }, []);

    useEffect(() => {
        userService.getAll().then((response) => {
            console.log('response', response);
            if (response.length > 0) {
                const users = response.filter((user) => user.address);
                setUsers(users);
            }
        });
    }, []);

    useEffect(() => {
        deviceService.getAll().then((response) => {
            console.log('response', response);
            setDevices(response);
        });
    }, []);

    return (
        <Box>
            <h1>Welcome {user.userName}</h1>
            <p>Username: {user.userName}</p>
            <p>Password: {user.password}</p>
            <p>Email: {user.email}</p>
            <p>Address: {user.address}</p>

            <Button onClick={handleOpen} variant="contained" color="primary">
                Create User
            </Button>
            <CreateUserDialog open={open} onClose={handleClose} />
            <UserTable users={users} />

            <Button onClick={handleOpenCreateDevice} variant="contained" color="primary">
                Create Device
            </Button>
            <CreateDeviceDialog open={openCreateDevice} onClose={handleCloseCreateDevice} />
            <DeviceTableAdmin devices={devices} />
        </Box>
    );
};

export default AdminHome;

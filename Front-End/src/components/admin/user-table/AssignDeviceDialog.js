
import React, { useState, useEffect } from 'react';
import { Button, Checkbox, Dialog, DialogTitle, DialogContent, DialogActions, TableContainer, Table, TableCell, TableHead, TableBody, TableRow, Paper } from '@mui/material';
import { makeStyles } from '@material-ui/core/styles';


import DeviceService from '../../../services/device.service';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });

const deviceService = new DeviceService();

const AssignDeviceDialog = ({ user, open, onClose }) => {
    const classes = useStyles();
    const [devices, setDevices] = useState([]);
    const [selectedDevices, setSelectedDevices] = useState([]);

    useEffect(() => {
        if (!user) {
            return;
        }
        deviceService.getAll().then((data) => {
            const availableDevices = data.filter((d) =>
                d.userId === '' || d.userId === user.id
            );
            setDevices(availableDevices);
            let userDevices = availableDevices.filter((d) => d.userId === user.id);
            setSelectedDevices(userDevices);
        });
    }, [user]);

    const handleCheckboxChange = (event, device) => {
        if (event.target.checked) {
            setSelectedDevices([...selectedDevices, device]);
        } else {
            setSelectedDevices(selectedDevices.filter((d) => d.id !== device.id));
        }
    };

    const handleSubmit = () => {
        // Handle form submission here
        console.log('Submitted:', { selectedDevices });
        const deviceIds = selectedDevices.map((d) => d.id);
        deviceService.assignDevicesToUser(user.id, deviceIds);
        onClose();
    };

    return (
    <Dialog open={open} onClose={onClose}>
        <DialogTitle>Edit user's devices</DialogTitle>
        <DialogContent>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="user table">
                    <TableHead>
                        <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>Description</TableCell>
                        <TableCell>Address</TableCell>
                        <TableCell>User owns device</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {devices.map((device) => (
                        <TableRow key={device.id}>
                            <TableCell component="th" scope="row">
                            {device.name}
                            </TableCell>
                            <TableCell>{device.description}</TableCell>
                            <TableCell>{device.address}</TableCell>
                            <TableCell>
                                <Checkbox
                                    checked={selectedDevices.some((d) => d.id === device.id)}
                                    onChange={(event) => handleCheckboxChange(event, device)}
                                />
                            </TableCell>
                        </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </DialogContent>
        <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                <Button onClick={handleSubmit}
                    color="primary">
                    Save
                </Button>
        </DialogActions>
    </Dialog>
    );
};

export default AssignDeviceDialog;


import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import Button from '@mui/material/Button'
import EditUserDialog from './EditUserDialog';
import AssignDeviceDialog from './AssignDeviceDialog';

import UserService from '../../../services/user.service';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const userService = new UserService();

const UserTable = ({ users }) => {
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);
    const [openAssignDevice, setOpenAssignDevice] = React.useState(false);
    const [selectedUser, setSelectedUser] = React.useState(null);

    const handleEditClick = (user) => {
        setSelectedUser(user);
        setOpen(true);
    };

    const handleDeleteClick = (user) => {
        userService.delete(user.id);
    };

    const handleAssignDeviceClick = (user) => {
        setSelectedUser(user);
        setOpenAssignDevice(true);
    };

    const handleClose = () => {
        setSelectedUser(null);
        setOpen(false);
    };

    const handleAssignDeviceClose = () => {
        setOpenAssignDevice(false);
    };

    return (
    <>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="user table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Address</TableCell>
              <TableCell>Password</TableCell>
              <TableCell>Edit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow key={user.id}>
                <TableCell component="th" scope="row">
                  {user.userName}
                </TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>{user.address}</TableCell>
                <TableCell>{user.password}</TableCell>
                <TableCell>
                    <Button variant="contained" color="primary" onClick={() => handleEditClick(user)}>
                        Edit
                    </Button>
                </TableCell>

                <TableCell>
                    <Button variant="contained" color="error" onClick={() => handleDeleteClick(user)}>
                        Delete
                    </Button>
                </TableCell>

                <TableCell>
                    <Button variant="contained" color="success" onClick={() => handleAssignDeviceClick(user)}>
                        Assign device
                    </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <EditUserDialog open={open} onClose={handleClose} user={selectedUser} />
      <AssignDeviceDialog open={openAssignDevice} onClose={handleAssignDeviceClose} user={selectedUser} />
    </>
  );
};

export default UserTable;
